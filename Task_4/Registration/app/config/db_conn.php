<?php
    $dbh = 'mysql:dbname=test; host=localhost';
    $usr = 'root';
    $pass = '';

    try{
        $conn = new PDO($dbh, $usr, $pass);
    }catch(PDOException $error){
        file_put_contents('error-pdo-connection.txt', $error->getMessage(), FILE_APPEND | LOCK_EX);
        die('Site down');
    }
