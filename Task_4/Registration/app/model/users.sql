DROP TABLE users;
CREATE TABLE users(
	id INT PRIMARY KEY AUTO_INCREMENT,
    usr_name VARCHAR(50),
    email VARCHAR(100) NOT NULL,
    usr_password VARCHAR(20) NOT NULL,
    gender VARCHAR(6),
	img VARCHAR(100)
);

DESCRIBE users;
SELECT * FROM users;
DELETE FROM users WHERE id = 3;
UPDATE users SET usr_name = 'mostafa', email = 'mostafa@gmail.com', gender = 'male' WHERE id = 3;
INSERT INTO users(usr_name, email, usr_password, gender) VALUES("merhan", 'merhan@gmail.com', 'admin', 'male');