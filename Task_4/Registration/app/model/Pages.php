<?php

class Page{
    public $name;

    // public function __construct($name){
    //     $this->name = $name;
    //     require_once "pages/$name.php";
    // }

    public function getPage($pg_name){
        switch($pg_name){
            case "login";
            case "signup";
            case "home";
            case "setting";
                require_once "pages/$pg_name.view.php";
                break;
            default:
                require_once 'pages/404.php';
        }
    }

    // public function login(){
    //     require_once "pages/login.view.php";
    // }

    // public function signup(){
    //     require_once 'pages/signup.view.php';
    // }

    // public function home(){
    //     require_once 'pages/home.view.php';
    // }

    // public function __call($name, $arguments)
    // {
    //     require_once 'pages/404.php';
    // }
}