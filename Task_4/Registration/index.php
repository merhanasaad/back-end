<?php
    require_once 'app/model/Pages.php';
    
    if (!empty($_GET['page'])) {
        $page = $_GET['page'];
    } else {
        $page = 'login';
    }

    $pageView = new Page();
    $pageView->getPage($page);
    
