<?php
    require_once "app/libs/auth.php";
    require_once "app/config/db_conn.php";
    //if no user log in 
    login();

    $id =  $_SESSION['user']['id'];
    $name = $_SESSION['user']['usr_name'];
    $email = $_SESSION['user']['email'];
    $password = $_SESSION['user']['usr_password'];
    $confirmPass = $password;
    $gender = $_SESSION['user']['gender'];

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        //validName
        $name = $_POST['user_name'];
        $email = $_POST['email'];
        $password = $_POST['password'];
        $confirmPass = $_POST['confirmedPassword'];
        $gender = $_POST['gender'];

        if(empty($name)){
            $name_err = "field is required";
        }else{
            if(preg_match("/^[a-zA-Z'-]+$/", $name)) {
                $name_err = "";
            }else{
                $name_err = "not valid name";
            }
        }
        //validEmail
        if(empty($email)){
            $email_err = "field is required";
        }else{
            if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $email_err = "";
            }else{
                $email_err = "not valid email";
            }
        }
        //validPassword
        if(!empty($password)){
            if (strlen($password) <= '6') {
                $password_err = "Your Password Must Contain At Least 8 Characters!";
            }
            elseif(!preg_match("#[0-9]+#",$password)) {
                $password_err = "Your Password Must Contain At Least 1 Number!";
            }
            elseif(!preg_match("#[A-Z]+#",$password)) {
                $password_err = "Your Password Must Contain At Least 1 Capital Letter!";
            }
            elseif(!preg_match("#[a-z]+#",$password)) {
                $password_err = "Your Password Must Contain At Least 1 Lowercase Letter!";
            }else{
                $password_err = "";
            }
        }else{
            $password_err = "field is required";
        }
        if(empty($confirmPass)){
            $confirmPass_err = "field is required";
        }else{
            if($password == $confirmPass){
                $confirmPass_err = "";
            }else{
                $confirmPass_err = "not matched";
            }
        }
        //validGender
        if(empty($gender)){
            $gender_err = "field is required";
        }else{
            $gender_err = "";
        }
    }

    //submit
    if(!empty($name) && $name_err === ""
    && !empty($email) && $email_err === ""
    && !empty($password) && $password_err === ""
    && !empty($confirmPass) && $confirmPass_err === ""){
        
        $newUser = $conn->prepare("UPDATE users SET usr_name = \"$name\" , email = \"$email\", gender = \"$gender\", usr_password = \"$password\"
        WHERE id = $id");
        $newUser->execute();

        $data = $conn->prepare("SELECT * FROM users WHERE id = $id");
        $data->execute();
        $users = $data->fetchAll(PDO::FETCH_ASSOC);

        unset( $_SESSION['user']);

        $_SESSION['user'] = $users[0];

        header("Location: index.php?page=home");
    }
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>setting</title>
    <link rel="stylesheet" href="pages/css/setting.css">
</head>
<body>
    <div class="container">
        <form method="post" enctype="multipart/form-data">
            <div class="imgCntainer">
                <img src="pages/uploads/defaultImg.png"
                 alt="User_Image" name="file" class="roundedImg"  accept="image/*">
                <br>
                <input type="file" name="file_img">
            </div>
            <p>Name </p>
            <input type="text" name="user_name" value="<?= $name;?>"
            class="<?php if($name_err){echo "notValid";}?>">
            <br>
            <span class="error"><?php if($name_err){
                echo "*".$name_err;
            }?></span>
            <br>

            <p>Email</p>
            <input type="text" name="email" value="<?php if($email){echo $email;}?>"
            class="<?php if($email_err){echo "notValid";}?>">
            <br>
            <span class="error"><?php if($email_err){
                echo "*".$email_err;
            }?></span>
            <br>

            <p>Password </p>
            <input type="password" name="password" value="<?php if($password){echo $password;}?>"
            class="<?php if($password_err){echo "notValid";}?>">
            <br>
            <span class="error"><?php if($password_err){
                echo "*".$password_err;
            }?></span>
            <br>

            <p>Confirm Password </p>
            <input type="password" name="confirmedPassword" value="<?php if($confirmPass){echo $confirmPass;}?>"
            class="<?php if($confirmPass_err){echo "notValid";}?>">
            <br>
            <span class="error"><?php if($confirmPass_err){
                echo "*".$confirmPass_err;
            }?></span>
            <br>

            <p>Gender </p>
            <input list="genders" name="gender" value="<?php if($gender){echo $gender;}else{echo "Male";}?>"
            class="<?php if($gender_err){echo "notValid";}?>">
            <datalist id="genders">
                <option value="Male">
                <option value="Female">
            </datalist>
            <br>

            <span class="error"><?php if($gender_err){
                echo "*".$gender_err;
            }?></span>            
            <br>
            <input type="submit"class="submit btnHover" value="Save">
            <a href="index.php?page=home" class="button btnHover">Back</a>  
        </form>
    </div>
</body>
</html>