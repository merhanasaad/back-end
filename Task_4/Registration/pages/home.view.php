<?php
    require_once "app/libs/auth.php";
    require_once "app/config/db_conn.php";
    //if no user log in 
    login();

    //logout button clicked
    if(isset($_GET['logout'])){
        unset($_SESSION['user']); 
        exit( header("Location: index.php?page=login"));
    }

    //delate button clicked
    if(isset($_GET['delete'])){
        $delete = $conn->prepare("DELETE FROM users WHERE id = :id");
        $delete->execute(['id' => $_SESSION['user']['id']]);
        echo 'deleted';
        unset($_SESSION['user']);
        exit( header("Location: index.php?page=login"));
    }

    $request = $conn->prepare("SELECT * FROM users");
    $request->execute();
    $users = $request->fetchAll(PDO::FETCH_ASSOC);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script src="https://kit.fontawesome.com/020da39d0c.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="pages/css/home.css">
</head>
<body>
    <div class="countaier">
        <aside>
            <div class="userInfoCard">
                <div class="userImgContainer">
                    <img src="<?php if($_SESSION['user']['img']){echo "./upload".$_SESSION['user']['img'];}else{echo "pages/uploads/defaultImg.png";}?>" alt="User_Image" name="user_Img">
                </div>
                <div class="usserInfo">
                    <h1><?= $_SESSION['user']['usr_name'] ?></h1>
                    <div class="userEmail"><?= $_SESSION['user']['email'] ?></div>
                </div>    
                <a href="index.php?page=setting" class="setting"> <i class="fas fa-cog"></i></a>  
            </div>
            <a href="index.php?page=home&logout=true" class="button">Log Out</a>  
        </aside>

        <div class="content">
            <table>
                <tr>
                    <th>id</th>
                    <th>usr_name</th>
                    <th>email</th>
                    <th>gender</th>
                </tr>
                <?php foreach($users as $user):?>
                    <?php if($user['email'] === $_SESSION['user']['email']):?>
                        <tr class='owner'>
                            <td><?= $user['id'];?></td>
                            <td><?= $user['usr_name'];?></td>
                            <td><?= $user['email'];?></td>
                            <td><?= $user['gender'];?></td>
                        </tr>
                    <?php else: ?>
                        <tr>
                            <td><?= $user['id'];?></td>
                            <td><?= $user['usr_name'];?></td>
                            <td><?= $user['email'];?></td>
                            <td><?= $user['gender'];?></td>
                        </tr>
                    <?php endif; ?>
                <?php endforeach; ?>
            </table>
        </div>
        
        <a href="index.php?page=home&delete=true" class='delete'>Delete Account</a>
    </div>

</body>
</html>
