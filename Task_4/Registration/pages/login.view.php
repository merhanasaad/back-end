<?php
    require_once "app/libs/auth.php";
    require_once "app/config/db_conn.php";

    //check if any user already login
    checkUser();

    $email = $_POST['email'];
    $password = $_POST['password'];


    if($_SERVER["REQUEST_METHOD"] == "POST"){
        if(empty($email) || empty($password)){
            $err = 'fill all field';
        }else{
            $request = $conn->prepare("SELECT * FROM `users` WHERE `email` = :email AND `usr_password` = :usr_password");
            $request->execute(['email' => $email, 'usr_password'=> $password]);
            $user = $request->fetchAll(PDO::FETCH_ASSOC);

            if(!$user){
                $err = 'uncorrect email or passwod';
            }else{
                $_SESSION['user'] = $user[0];
                exit( header("Location: index.php?page=home"));
            }

        }
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Log In</title>
    <link rel="stylesheet" href="pages/css/index.css">
</head>
<body>
    <div class="container">
        <form method="post" name="signIn">
            <h3> Sign In</h3>
            <span class="error"><?php if($err){
                echo "*".$err;
            }?></span>
            <p>Email  </p>
            <input type="text" name="email" value="<?php if($email){echo $email;}?>"
            class="<?php if($email_err){echo "notValid";}?>">
            <br>
            <p>Password </p>
            <input type="password" name="password" value="<?php if($password){echo $password;}?>"
            class="<?php if($password_err){echo "notValid";}?>">
            <span class="error"><?php if($password_err){
                echo "*".$password_err;
            }?></span>
            <input type="submit" class="submit">
        </form>
        <p>Don't have account <a href="index.php?page=signup">Sign UP</a></p>
    </div>
</body>
</html>