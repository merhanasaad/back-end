<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>login_form</title>
    <link rel="stylesheet" href="index.css">
</head>
<body>
    <div class="container">
        <form action="home.php" method="get" name="signUp">
            <h3> Sign Up</h3>
            <p>Name </p>
            <input type="text" name="name" required>
            <p>Email  </p>
            <input type="email" name="email" required>
            <p>Password </p>
            <input type="password" name="password" minlength="6" required>
            <p>Confirm Password </p>
            <input type="password" name="confirmedPassword" minlength="6" required>
            <p>Gender </p>
            <input list="genders" name="gender" required>
            <datalist id="genders">
                <option value="Male">
                <option value="Female">
            </datalist>
            <br>
            <input type="submit" class="submit" onclick="correctPassword(); validateEmail()">
        </form>
    </div>


    <script>
        var form = document.forms["signUp"];
        var email = form["email"];
        var password = form["password"];
        var confirmedPassword = form["confirmedPassword"];

        function validatePassword() {
            if(password.value !== confirmedPassword.value){
                confirmedPassword.setCustomValidity("Passwords Don't Match");
                confirmedPassword.classList.add("notValid");
            }else{
                confirmedPassword.setCustomValidity('');
                confirmedPassword.classList.remove("notValid");
            }
        }

        function validateEmail() {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            if(re.test(email.value.toLowerCase())){
                email.classList.remove("notValid");
                email.setCustomValidity('');
            }else{
                email.setCustomValidity("email not valid");
                email.classList.add("notValid");
            }
        }

        email.onchange = validateEmail;
        password.onchange = validatePassword;
        confirmedPassword.onkeyup = validatePassword;
    </script>
</body>
</html>