<?php
    require_once "shape.php";

    class Circle extends Shape{
        public const SHAPE_TYPE = 3;
        protected $radius;

        public function __construct($radius){
            parent::__construct(0, 0);
            $this->radius = $radius;
        }

        public function area(){
            return (2 * 3.14 * $this->radius);
        }

        public function getFullDescription(){
            return "Circle " . " < " . "#" . $this->get_id() . " > : " . $this->name  . " - " . $this->radius;
        }
    }