<?php 
    class Shape{
        public const SHAPE_TYPE = 1;
        public $name;
        protected $length, $width;
        private $id;

        public function __construct($length, $width){
            $this->id = uniqid();
            $this->length = $length;
            $this->width = $width;
        }

        public function set_name($name){
            $this->name = $name;
        }

        public function get_name(){
            return $this->name;
        }

        public function get_id(){
            return $this->id;
        }

        public function area(){
            return ($this->length + $this->width) * 2;
        }

        public static function getTypeDescription(){
            return 'Type: ' . static::SHAPE_TYPE;
        }

        public function getFullDescription(){
            return "Shape " . "<" . "#" . $this->get_id() . "> : " . $this->get_name() . " - " . $this->length . " x " . $this->width;
        }
    }