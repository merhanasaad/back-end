<?php
    require_once 'app/model/shape.php';
    require_once 'app/model/rectangular.php';
    require_once 'app/model/circle.php';

    $shape = new Shape(3, 4);
    $shape->set_name(shape);
    echo "____shap____";
    echo "<br/>";
    echo $shape->area();
    echo "<br/>";
    echo $shape->get_id();
    echo "<br/>";
    echo $shape->get_name();
    echo "<br/>";
    echo($shape->getFullDescription());
    echo "<br/>";
    echo(Shape::getTypeDescription());

    echo "<br/>";

    $cirl = new Circle(3);
    $cirl->set_name(circle);
    echo "____circle____";
    echo "<br/>";
    echo $cirl->area();
    echo "<br/>";
    echo $cirl->get_id();
    echo "<br/>";
    echo $cirl->get_name();
    echo "<br/>";
    echo($cirl->getFullDescription());
    echo "<br/>";
    echo(Circle::getTypeDescription());

    echo "<br/>";

    $rect = new Rectangular(5, 1);
    $rect->set_name(rectangular);
    echo "____Rectangular____";
    echo "<br/>";
    echo $rect->area();
    echo "<br/>";
    echo $rect->get_id();
    echo "<br/>";
    echo $rect->get_name();
    echo "<br/>";
    echo($rect->getFullDescription());
    echo "<br/>";
    echo(Rectangular::getTypeDescription());
