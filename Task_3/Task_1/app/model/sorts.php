<?php
    class Sorts{
        private $arr;

        public function __construct(array $arr){
            $this->arr = $arr;
        }

        public function sorting(){
            sort($this->arr);
            return $this->arr;
        }

    }