<?php
    class MyCalculator{
        private $arg1, $arg2;

        public function __construct($arg1, $arg2){
            $this->arg1 = $arg1;
            $this->arg2 = $arg2;
        }

        public function add(){
            return $this->arg1 + $this->arg2;
        }

        public function multiply(){
            return $this->arg1 * $this->arg2;
        }

        public function subtract(){
            return $this->arg1 - $this->arg2;
        }

        public function divide(){
            return $this->arg1 / $this->arg2;
        }
        
    }