<?php
    require_once 'app/model/sorts.php';
    require_once 'app/model/calculator.php';

    $calc = new MyCalculator(12, 6);
    echo "_______calculator_________";
    echo "<br/>";
    echo "sum: " . $calc->add();
    echo "<br/>";
    echo "divide: " . $calc->divide();
    echo "<br/>";
    echo "multiply: " . $calc->multiply();
    echo "<br/>";
    echo "subtract: " . $calc->subtract();

    echo "<br/>";
    echo "_______sort_________";
    echo "<br/>";
    $sort = new Sorts([11, -2, 4, 35, 0, 8, -9]);
    print_r($sort->sorting());
