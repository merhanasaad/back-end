<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign Up</title>
    <link rel="stylesheet" href="./css/index.css">
</head>
<body>
    <?php 
        session_start();

        $_SESSION["name"] = $name = $_POST["user_name"];
        $_SESSION["email"]  = $email = $_POST["email"];
        $_SESSION["password"]  = $password = $_POST["password"];
        $_SESSION["confirmPass"] = $confirmPass = $_POST["confirmedPassword"];
        $_SESSION["gender"] = $gender = $_POST["gender"];

        $name_err = "";
        $email_err = "";
        $password_err = "";
        $confirmPass_err = "";
        $gender_err = "";

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            //validName
            if(empty($name)){
                $name_err = "field is required";
            }else{
                if(preg_match("/^[a-zA-Z'-]+$/", $name)) {
                    $name_err = "";
                }else{
                    $name_err = "not valid name";
                }
            }
            //validEmail
            if(empty($email)){
                $email_err = "field is required";
            }else{
                if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $email_err = "";
                }else{
                    $email_err = "not valid email";
                }
            }
            //validPassword
            if(!empty($password)){
                if (strlen($password) <= '6') {
                    $password_err = "Your Password Must Contain At Least 8 Characters!";
                }
                elseif(!preg_match("#[0-9]+#",$password)) {
                    $password_err = "Your Password Must Contain At Least 1 Number!";
                }
                elseif(!preg_match("#[A-Z]+#",$password)) {
                    $password_err = "Your Password Must Contain At Least 1 Capital Letter!";
                }
                elseif(!preg_match("#[a-z]+#",$password)) {
                    $password_err = "Your Password Must Contain At Least 1 Lowercase Letter!";
                }else{
                    $password_err = "";
                }
            }else{
                $password_err = "field is required";
            }
            if(empty($confirmPass)){
                $confirmPass_err = "field is required";
            }else{
                if($password == $confirmPass){
                    $confirmPass_err = "";
                }else{
                    $confirmPass_err = "not matched";
                }
            }
            //validGender
            if(empty($gender)){
                $gender_err = "field is required";
            }else{
                $gender_err = "";
            }

        }

        //submit
        if(!empty($name) && $name_err === ""
            && !empty($email) && $email_err === ""
            && !empty($password) && $password_err === ""
            && !empty($confirmPass) && $confirmPass_err === ""
            && !empty($gender) && $gender_err === ""){
            header("Location: setting.php");
        }
    ?>

    <div class="container">
        <form method="post" name="signUp">
            <h3> Sign Up</h3>
            <p>Name </p>
            <input type="text" name="user_name" value="<?php if($name){echo $name;}?>"
            class="<?php if($name_err){echo "notValid";}?>">
            <br>
            <span class="error"><?php if($name_err){
                echo "*".$name_err;
            }?></span>
            <p>Email  </p>
            <input type="text" name="email" value="<?php if($email){echo $email;}?>"
            class="<?php if($email_err){echo "notValid";}?>">
            <span class="error"><?php if($email_err){
                echo "*".$email_err;
            }?></span>
            <br>
            <p>Password </p>
            <input type="password" name="password" value="<?php if($password){echo $password;}?>"
            class="<?php if($password_err){echo "notValid";}?>">
            <span class="error"><?php if($password_err){
                echo "*".$password_err;
            }?></span>
            <br>
            <p>Confirm Password </p>
            <input type="password" name="confirmedPassword" value="<?php if($confirmPass){echo $confirmPass;}?>"
            class="<?php if($confirmPass_err){echo "notValid";}?>">
            <span class="error"><?php if($confirmPass_err){
                echo "*".$confirmPass_err;
            }?></span>
            <br>
            <p>Gender </p>
            <input list="genders" name="gender" value="<?php if($gender){echo $gender;}?>"
            class="<?php if($gender_err){echo "notValid";}?>">
            <datalist id="genders">
                <option value="Male">
                <option value="Female">
            </datalist>
            <br>
            <span class="error"><?php if($gender_err){
                echo "*".$gender_err;
            }?></span>            
            <br>
            <input type="submit" class="submit" name="submit">
        </form>
        <p>already have account <a href="./index.php">Sign In</a></p>
    </div>

</body>
</html>