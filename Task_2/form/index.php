<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Log In</title>
    <link rel="stylesheet" href="./css/index.css">
</head>
<body>
<?php 
        session_start();
        $fixed_user = ["user_email" => "admin@gmail.com", "password" => "S1234s12"];

       $email = $_SESSION['email'] = $_POST["email"];
       $password =  $_SESSION['password'] = $_POST["password"];

        $email_err = "";
        $password_err = "";

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            //validEmail
            if(empty($email)){
                $email_err = "field is required";
            }elseif(empty($password)){
                $password_err = "field is required";
            }
        }

        //submit
        if(!empty($email) && $email_err === ""
            && !empty($password) && $password_err === ""){
                if($email === $fixed_user["user_email"] && $password === $fixed_user["password"]){
                    header("Location: setting.php");
                }else{
                    if($email !== $fixed_user["user_email"]){
                        $email_err = "invalid email";
                    }else if($password !== $fixed_user["password"]){
                        $password_err = "passsword is not correct";
                    }
                }
            
        }
    ?>
    <div class="container">
        <form method="post" name="signIn">
            <h3> Sign In</h3>
            <p>Email  </p>
            <input type="text" name="email" value="<?php if($email){echo $email;}?>"
            class="<?php if($email_err){echo "notValid";}?>">
            <span class="error"><?php if($email_err){
                echo "*".$email_err;
            }?></span>
            <br>
            <p>Password </p>
            <input type="password" name="password" value="<?php if($password){echo $password;}?>"
            class="<?php if($password_err){echo "notValid";}?>">
            <span class="error"><?php if($password_err){
                echo "*".$password_err;
            }?></span>
            <input type="submit" class="submit" onclick="correctPassword(); validateEmail()">
        </form>
        <p>Don't have account <a href="./signUp.php">Sign UP</a></p>
    </div>
</body>
</html>