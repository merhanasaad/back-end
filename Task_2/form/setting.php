
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>setting</title>
    <link rel="stylesheet" href="./css/setting.css">
</head>
<body>
    <?php
        session_start();

        $name = $_SESSION['name'];
        if(!$name){
            $name = $_SESSION['name'] = $_POST["user_name"];
        }
        $email = $_SESSION['email'];
        
        $password = $_SESSION['password'];
        $confirmPass = $password;
        

        $gender = $_SESSION['gender'];
        if(!$gender){
            $gender = $_SESSION['gender'] = $_POST["gender"];
        }
        $imagepath = "";

        $image_name= $_FILES['file_img']['name'];
        $img_exsist = false;

        $img_err = [];
        $name_err = "";
        $email_err = "";
        $password_err = "";
        $confirmPass_err = "";

        if ($_SERVER["REQUEST_METHOD"] == "POST") {
            //validName
            if(empty($name)){
                $name_err = "field is required";
            }else{
                if(preg_match("/^[a-zA-Z'-]+$/", $name)) {
                    $name_err = "";
                }else{
                    $name_err = "not valid name";
                }
            }
            //validEmail
            if(empty($email)){
                $email_err = "field is required";
            }else{
                if(filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $email_err = "";
                }else{
                    $email_err = "not valid email";
                }
            }
            //validPassword
            if(!empty($password)){
                if (strlen($password) <= '6') {
                    $password_err = "Your Password Must Contain At Least 8 Characters!";
                }
                elseif(!preg_match("#[0-9]+#",$password)) {
                    $password_err = "Your Password Must Contain At Least 1 Number!";
                }
                elseif(!preg_match("#[A-Z]+#",$password)) {
                    $password_err = "Your Password Must Contain At Least 1 Capital Letter!";
                }
                elseif(!preg_match("#[a-z]+#",$password)) {
                    $password_err = "Your Password Must Contain At Least 1 Lowercase Letter!";
                }else{
                    $password_err = "";
                }
            }else{
                $password_err = "field is required";
            }
            if(empty($confirmPass)){
                $confirmPass_err = "field is required";
            }else{
                if($password == $confirmPass){
                    $confirmPass_err = "";
                }else{
                    $confirmPass_err = "not matched";
                }
            }
            //validGender
            if(empty($gender)){
                $gender_err = "field is required";
            }else{
                $gender_err = "";
            }

            //validImg
            if($image_name){
                $temp = explode(".", $image_name);

                //check if file is img 
                $check = getimagesize($_FILES["file_img"]["tmp_name"]);
                if($check !== false) {
                    $img_exsist = true;
                } else {
                    $img_err[] = "File is not an image";
                    $img_exsist = false;
                }

                $allowed_image_extension = array(
                    "png",
                    "jpg",
                    "jpeg"
                );
                
                $file_extension = pathinfo($_FILES["file_img"]["name"], PATHINFO_EXTENSION);

                if (!in_array($file_extension, $allowed_image_extension)) {
                    $img_err[] = "wrong extension";
                    $img_exsist = false;
                }

                if($img_exsist){
                    $temp = explode(".", $image_name);
                    $_SESSION['imagepath'] = $imagepath="uploads/".$image_name;
                    move_uploaded_file($_FILES["file_img"]["tmp_name"],$imagepath);
                    $img_exsist = true;
                }else{
                    $img_err[] = "img not exsist";
                }
            }else{
                $img_err[] = "choose Img";
            }
        }


        //submit
        if($image_name && !count($img_err)
            && !empty($name) && $name_err === ""
            && !empty($email) && $email_err === ""
            && !empty($password) && $password_err === ""
            && !empty($confirmPass) && $confirmPass_err === ""){
            header("Location: home.php");
        }
    ?>

    <div class="container">
        <form method="post" enctype="multipart/form-data">
            <div class="imgCntainer">
                <img src="<?php if($imagepath){echo "./".$imagepath;}else{echo "./images/defaultImg.png";}?>"
                 alt="User_Image" name="file" class="roundedImg"  accept="image/*">
                <br>
                <input type="file" name="file_img">
            </div>
            <?php if($img_err):
                for($i = 0 ; $i < count($img_err); $i++): ?>
                    <span class="error">
                        <?= "*".$img_err[$i]?>
                        <?= "<br>"?>
                    </span>
                <?php endfor; ?>
            <?php endif;?>

            <p>Name </p>
            <input type="text" name="user_name" value="<?php if($name){echo $name;}else{echo "admin";}?>"
            class="<?php if($name_err){echo "notValid";}?>">
            <br>
            <span class="error"><?php if($name_err){
                echo "*".$name_err;
            }?></span>
            <br>

            <p>Email</p>
            <input type="text" name="email" value="<?php if($email){echo $email;}?>"
            class="<?php if($email_err){echo "notValid";}?>">
            <br>
            <span class="error"><?php if($email_err){
                echo "*".$email_err;
            }?></span>
            <br>

            <p>Password </p>
            <input type="password" name="password" value="<?php if($password){echo $password;}?>"
            class="<?php if($password_err){echo "notValid";}?>">
            <br>
            <span class="error"><?php if($password_err){
                echo "*".$password_err;
            }?></span>
            <br>

            <p>Confirm Password </p>
            <input type="password" name="confirmedPassword" value="<?php if($confirmPass){echo $confirmPass;}?>"
            class="<?php if($confirmPass_err){echo "notValid";}?>">
            <br>
            <span class="error"><?php if($confirmPass_err){
                echo "*".$confirmPass_err;
            }?></span>
            <br>

            <p>Gender </p>
            <input list="genders" name="gender" value="<?php if($gender){echo $gender;}else{echo "Male";}?>"
            class="<?php if($gender_err){echo "notValid";}?>">
            <datalist id="genders">
                <option value="Male">
                <option value="Female">
            </datalist>
            <br>

            <span class="error"><?php if($gender_err){
                echo "*".$gender_err;
            }?></span>            
            <br>
            <input type="submit"class="submit btnHover" value="Save">
            <!-- <a href="./home.php" class="button btnHover">Back</a>   -->
        </form>
    </div>
</body>
</html>