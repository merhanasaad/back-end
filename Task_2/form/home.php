<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <script src="https://kit.fontawesome.com/020da39d0c.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="./css/home.css">
</head>
<body>
    <?php
        session_start();

        $name = $_SESSION['name'];
        $email = $_SESSION['email'];
        $password = $_SESSION['password'];
        $confirmPass = $_SESSION['confirmedPassword'];
        $gender = $_SESSION['gender'];
        $imagepath = $_SESSION['imagepath'];
    ?>

    <div class="countaier">
        <aside>
            <div class="userInfoCard">
                <div class="userImgContainer">
                    <img src="<?php if($imagepath){echo "./".$imagepath;}else{echo "./images/defaultImg.png";}?>" alt="User_Image" name="user_Img">
                </div>
                <div class="usserInfo">
                    <h1><?= $name ?></h1>
                    <div class="userEmail"><?= $email ?></div>
                </div>    
                <a href="./setting.php" class="setting"> <i class="fas fa-cog"></i></a>  
            </div>
            <a href="./index.php" class="button">Log Out</a>  
        </aside>
    </div>

</body>
</html>
