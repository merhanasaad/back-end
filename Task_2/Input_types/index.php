<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>input_types</title>
    <style>
        form{
            width: 50%;
            display: block;
            margin: auto;
        }
    </style>
</head>
<body>
    <form method="post" action="data.php">
        You can choose more than one Day:
        <br><br>
        <input type="checkbox" name="days[]" value= "sat">sat 
        <input type="checkbox" name="days[]" value= "sun">sun 
        <input type="checkbox" name="days[]" value= "mon">mon 
        <input type="checkbox" name="days[]" value= "tus">tus 
        <input type="checkbox" name="days[]" value= "wend">wend 
        <input type="checkbox" name="days[]" value= "fri">fri 
        <hr>
        Color: <input type="color" name="color">
        <hr>
        Date: <input type="date" name="date">
        <hr>
        Choose one Week Day:
        <br><br>
        <input type="radio" name="week_days" value= "sat">sat 
        <input type="radio" name="week_days" value= "sun">sun 
        <input type="radio" name="week_days" value= "mon">mon 
        <input type="radio" name="week_days" value= "tus">tus 
        <input type="radio" name="week_days" value= "wend">wend 
        <input type="radio" name="week_days" value= "fri">fri 
        <br><hr><br>
        Week: <input type="week" name="week">
        <br><hr><br>
        Time: <input type="time" name="time">
        <br><hr><br>
        URL: <input type="url" name="url">
        <br><hr><br>
        telephone: <input type="tel" name="tel">
        <br><hr><br>
        Search: <input type="search" name="search">
        <br><hr><br>
        Rande: <input type="range" name="range">
        <br><hr><br>
        <input type="reset" value="reset">
        
        <input type="submit" value="print">
    </form>
</body>
</html>
